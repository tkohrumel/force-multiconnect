@isTest
private class HerokuConnectSetup_Test
{

    static testMethod void installTest()
    {
        HerokuConnectSetup postinstall = new HerokuConnectSetup();
        Test.testInstall(postinstall, new Version(1, 0), true);
    }
}
