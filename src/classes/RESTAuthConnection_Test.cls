@isTest
private class Test_RESTAuthConnection {

	@isTest static void test_doPost() {
		RestRequest req = new RestRequest();
		RestResponse resp = new RestResponse();

		req.requestURI = 'https://na30.salesforce.com/services/apexrest/AuthConnection/';
		req.httpMethod = 'POST';

		RESTAuthConnection result = RESTAuthConnection.doPost(req, resp);

		System.assert(result != null);
	}
}
