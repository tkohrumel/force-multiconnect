@IsTest public with sharing class AuthController_Test {
    static testMethod void test_positive_getAuthUrl() {
        Connection__c connection = new Connection__c();
        connection.auth_url__c = 'http://foo.test.com/auth';
        
        insert connection;
        
        AuthController controller = new AuthController();
        System.assertEquals(connection.auth_url__c, controller.getAuthUrl());  
    }
    
    static testMethod void test_negative_getAuthUrl() {
        AuthController controller = new AuthController();
        System.assertEquals('', controller.getAuthUrl());
    }     
}