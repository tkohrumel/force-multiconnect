global without sharing class HerokuConnectSetup implements InstallHandler {
    public static HEROKU_CONFIG__c heroku_config = HEROKU_CONFIG__c.getInstance('Heroku Connect');

    global void onInstall(InstallContext context) {
      if(context.isUpgrade()){
        return;
      }

      HttpRequest request = new HttpRequest();
      HttpResponse response = new HttpResponse();
      Http http = new Http();

      User[] users = [SELECT email FROM User WHERE id = :context.installerId()];

      User admin = (users.size() > 0) ? users[0] : null;

      String endpoint = 'https://screeningforce-staging.herokuapp.com/organizations';

      request.setMethod('POST');
      request.setEndpoint(endpoint);
      request.setHeader('Content-Type', 'application/json');
      request.setHeader('Accept', 'application/json');

      request.setBody('{"org_id":"'+ context.organizationId()+ '", "admin_email":"' +admin.email+ '", "org_name":"' +UserInfo.getOrganizationName()+ '"}');

      System.debug('=========== request body: ' + request.getBody());

      try {
          response = http.send(request);
          System.debug(response.toString());
          System.debug(response.getBody());
          System.debug(response.getStatus());
      } catch (System.CalloutException e) {
          System.debug('---Callout error: ' + e);
          System.debug(response.toString());
          System.debug(response.getBody());
          System.debug(response.getStatus());
      }
    }
}
