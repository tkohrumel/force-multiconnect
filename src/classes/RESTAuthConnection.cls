@RestResource(urlMapping='/AuthConnection/*')
global with sharing class RESTAuthConnection {

@HttpPost   
  global static void doPost(String connectionId, String authorizeUrl) {
      Heroku_Connect__c connectionSetting = Heroku_Connect__c.getInstance('connection');

      System.debug('------------- SUCCESS');
      System.debug('------------- connectionId: ' + connectionId);
      System.debug('------------- authorizeUrl: ' + authorizeUrl);
      connectionSetting.Connection_ID__c = connectionId;
      connectionSetting.isReadyToAuth__c = true;
      
      update connectionSetting;
      
      // move to batch apex
      
      // if any connection exists, perform upsert
      
      Connection__c connection = ([SELECT COUNT() FROM connection__c] > 0) ? [SELECT auth_url__c FROM connection__c LIMIT 1] : new connection__c();
      
      connection.auth_url__c = authorizeUrl;
      
      upsert connection;
  }
}